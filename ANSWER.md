---------------------This project used SQL ORACLE DEVELOPER (Version Oracle IDE	18.3.0.277.2354) to complete--------------------

--CREATE TABLE NAMED: USERS 
CREATE TABLE users (user_id int, age varchar(5), country varchar(2));

--CREATE TABLE NAMED: user_interaction
CREATE TABLE user_interaction (event_id varchar(36), user_id int, event_type varchar(5), event_time VARCHAR(200));

DROP TABLE user_interaction;
COMMIT;

--INSERT VALUES INTO USERS TABLE:
INSERT ALL
INTO users VALUES ('3', '18-45', 'DE')
INTO users VALUES ('1', '46-80', 'DE')
INTO users VALUES ('45', '46-80', 'UK')
INTO users VALUES ('100', '18-45', 'UK')
SELECT 1 FROM DUAL;
COMMIT;

--INSERT VALUES INTO user_interaction TABLE:
INSERT ALL
INTO user_interaction VALUES ( 'C301272F-1D61-01DB-4855-F2715E149D4F', '3', 'CLICK', '2019-05-05 14:05:20')
INTO user_interaction VALUES ( '86589C52-451B-3763-7266-140292B18054', '3', 'VIEW', '2019-05-05 14:05:21')
INTO user_interaction VALUES ( '75C813B4-2363-4DE0-0D0C-3D6968D66F4B', '3', 'CLICK', '2019-05-05 14:05:22')
INTO user_interaction VALUES ( 'F36AABE3-0306-6669-36F0-5E6C255553FD', '45', 'VIEW', '2019-05-05 14:05:23')
INTO user_interaction VALUES ( '5F9CF4D8-6D12-331C-1224-109D188E9918', '45', 'EXIT', '2019-05-05 14:05:24')
INTO user_interaction VALUES ( 'F36AABE3-0306-6669-36F0-5E6C255553FD', '45', 'VIEW', '2019-05-05 14:05:23')
INTO user_interaction VALUES ( '5F9CF4D8-6D12-331C-1224-109D188E9918', '45', 'EXIT', '2019-05-05 14:05:24')
INTO user_interaction VALUES ( '195DE777-9534-4AFF-7A0A-85E2254328A8', '1', 'CLICK', '2019-05-05 14:05:25')
INTO user_interaction VALUES ( '525631F6-43D0-5E38-84D4-71A5540A6140', '100', 'CLICK', '2019-05-05 14:05:26')
INTO user_interaction VALUES ( '260C93F9-1BE3-4B35-99EA-9D78BBE3B63D', '80', 'CLICK', '2019-05-05 14:10:26')
INTO user_interaction VALUES ( 'FB523818-7A13-5C8E-A6AF-67AC34B21240', '80', 'VIEW', '2019-05-05 14:10:29')
INTO user_interaction VALUES ( '4D94DFA3-E839-4EFE-A104-A745558BD570', '80', 'VIEW', '2019-05-05 14:10:31')
INTO user_interaction VALUES ( 'FECC8670-8BAE-40E7-92DA-4A53CF36169B', '100', 'CLICK', '2019-06-05 09:05:12')
INTO user_interaction VALUES ( '94C331D8-B6BD-4D77-A09F-54B32E99EC95', '100', 'VIEW', '2019-06-05 09:05:13')
INTO user_interaction VALUES ( '634F58B8-52D9-4347-B747-608A3B2C2D90', '100', 'CLICK', '2019-06-05 09:05:16')
INTO user_interaction VALUES ( '9C82ADA3-85EC-496A-A645-297024A96394', '100', 'EXIT', '2019-06-05 09:05:21')
INTO user_interaction VALUES ( '10DEE4FD-C9BF-4623-9DBB-F2554E5F2701', '1', 'CLICK', '2019-07-05 10:05:25')
INTO user_interaction VALUES ( '796598E3-C2AA-42C3-869D-7D741EF4F614', '1', 'CLICK', '2019-07-05 10:05:26')
INTO user_interaction VALUES ( 'BA19CE1B-8103-4F81-81B5-EBFE62BAA024', '1', 'CLICK', '2019-07-05 10:05:26')
INTO user_interaction VALUES ( 'B62AD5FF-045E-4298-A94A-647F1C7C1AB2', '1', 'VIEW', '2019-07-05 10:05:29')
INTO user_interaction VALUES ( '97793862-BA26-4459-BEA3-01273DFDF00D', '1', 'CLICK', '2019-07-05 10:05:31')
INTO user_interaction VALUES ( '28B430CB-D8CF-4D1D-9B09-F7161739E139', '1', 'EXIT', '2019-07-05 10:05:46')
INTO user_interaction VALUES ( '59E23145-475F-44ED-AC6A-443A9E60E0E3', '3', 'CLICK', '2019-07-06 10:05:10')
INTO user_interaction VALUES ( 'A41CC81D-1CB2-42CE-9C42-BF7F2E307C76', '3', 'CLICK', '2019-07-06 10:05:14')
INTO user_interaction VALUES ( 'FF46EA4A-5D26-4CA1-ADC4-5E57C4619E53', '3', 'VIEW', '2019-07-06 10:05:16')
INTO user_interaction VALUES ( '1D339486-03AB-4A74-A60F-530BBB6D1D24', '3', 'CLICK', '2019-07-06 10:05:17')
INTO user_interaction VALUES ( 'E3FE6DE4-CA64-4688-A723-D0FF2FEFB4F3', '3', 'CLICK', '2019-07-06 10:05:32')
INTO user_interaction VALUES ( '42884AA2-1648-4E4F-B3CE-2194862009CF', '3', 'CLICK', '2019-07-06 10:05:35')
INTO user_interaction VALUES ( '64B0EC1D-F97E-4CF5-A3B0-CFD0634154B2', '3', 'CLICK', '2019-07-06 10:05:36')
INTO user_interaction VALUES ( '4F7A208C-7EA7-407D-9AE0-C973BDE2DAB4', '3', 'VIEW', '2019-07-06 10:05:39')
INTO user_interaction VALUES ( '4AD71C8D-1760-40B3-86E2-921B849200E7', '3', 'CLICK', '2019-07-06 10:05:47')
INTO user_interaction VALUES ( 'AF11C959-C9F4-4FF5-AF56-3774AB5A38B9', '3', 'EXIT', '2019-07-06 10:05:57')
INTO user_interaction VALUES ( '640A2960-C747-4C2E-90FB-D4FB39A2ECAB', '1', 'CLICK', '2019-07-07 10:05:13')
INTO user_interaction VALUES ( '2CA55A2D-96D1-4C03-B7E8-7381E046085A', '1', 'VIEW', '2019-07-07 10:05:23')
INTO user_interaction VALUES ( 'EF9C72B4-5A48-4019-9D39-B983CEE3A3C2', '1', 'CLICK', '2019-07-07 10:05:25')
INTO user_interaction VALUES ( 'D3525461-14A4-4591-8131-0AD4B88DD1CF', '1', 'VIEW', '2019-07-07 10:05:25')
INTO user_interaction VALUES ( '4C0C7FBE-0359-4BFB-8522-4796FD591B1A', '1', 'CLICK', '2019-07-07 10:05:29')
INTO user_interaction VALUES ( 'D12DEBB5-7062-4EDD-9AE5-A7E13AB9F4B6', '1', 'CLICK', '2019-07-07 10:05:37')
INTO user_interaction VALUES ( '0FDD7F05-BBAA-4B2D-A264-87C4189E5C54', '1', 'CLICK', '2019-07-07 10:05:49')
INTO user_interaction VALUES ( 'CD0874BF-FA20-459E-A61E-78D670EB5D0B', '1', 'CLICK', '2019-07-07 10:05:59')
INTO user_interaction VALUES ( 'E19F3DBD-57F6-46B1-AA73-1C67426C0638', '1', 'CLICK', '2019-07-07 10:06:01')
INTO user_interaction VALUES ( '2ED24A11-A611-490E-A488-8628D919B2F0', '1', 'VIEW', '2019-07-07 10:06:03')
INTO user_interaction VALUES ( 'FA8FEF23-54E8-4B93-BDDD-40F79BFAF898', '1', 'CLICK', '2019-07-07 10:06:09')
INTO user_interaction VALUES ( '4C87381A-1463-462C-BCF1-2949050CCA11', '1', 'CLICK', '2019-07-07 10:06:14')
INTO user_interaction VALUES ( 'C7C41415-F116-4A69-AF71-96084A643D79', '1', 'VIEW', '2019-07-07 10:06:25')
INTO user_interaction VALUES ( '0470F2D6-EFDC-4016-B03B-936160FBC01C', '1', 'CLICK', '2019-07-07 10:06:29')
INTO user_interaction VALUES ( 'DDA597A0-822B-4232-B39F-653873C2C51F', '1', 'CLICK', '2019-07-07 10:06:45')
INTO user_interaction VALUES ( 'E9F3C025-3272-48F8-B319-B877C64E3CBE', '1', 'CLICK', '2019-07-07 10:06:46')
INTO user_interaction VALUES ( '62CB6AB9-B222-4864-9024-80B457F297E5', '1', 'VIEW', '2019-07-07 10:06:47')
INTO user_interaction VALUES ( '23781FE0-FD3B-41BC-801C-0B576B4549CB', '1', 'CLICK', '2019-07-07 10:06:50')
INTO user_interaction VALUES ( '12EDC97B-8B8A-4890-8AB1-8026037B0AC8', '1', 'EXIT', '2019-07-07 10:06:55')
INTO user_interaction VALUES ( 'AC63F5DC-AD52-40AA-BF89-2B2D6F6950DD', '1', 'CLICK', '2019-08-05 10:05:25')
INTO user_interaction VALUES ( '8FF65572-F070-48E6-86F3-19C000E33D68', '1', 'CLICK', '2019-08-05 10:05:26')
INTO user_interaction VALUES ( 'E91E825B-7E12-418B-8CD4-8F90AE70B355', '1', 'CLICK', '2019-08-05 10:05:26')
INTO user_interaction VALUES ( 'CC82DEFD-CF7F-4ED5-B494-6BA6220C5C54', '1', 'VIEW', '2019-08-05 10:05:29')
INTO user_interaction VALUES ( 'A846CE52-58A5-411B-972C-B5511C0662EB', '1', 'CLICK', '2019-08-05 10:05:31')
INTO user_interaction VALUES ( 'F0B18BA7-7FB0-47A2-BBE5-7C61B51FA62A', '1', 'EXIT', '2019-08-05 10:05:46')
INTO user_interaction VALUES ( 'B2E1194E-1F38-4EEA-85B9-00C74D509AF5', '3', 'CLICK', '2019-08-06 10:05:10')
INTO user_interaction VALUES ( '538C5C6F-5269-427C-B9E2-8779FA32CF74', '3', 'CLICK', '2019-08-06 10:05:14')
INTO user_interaction VALUES ( '544E54BD-B483-4862-901F-B3B7DF08E5AC', '3', 'VIEW', '2019-08-06 10:05:16')
INTO user_interaction VALUES ( 'D8FA1BF4-9DD6-4F63-B23C-CCE0C453B1CB', '3', 'CLICK', '2019-08-06 10:05:17')
INTO user_interaction VALUES ( 'D8047EB1-6B1F-4D28-8E91-DA6D3B8574AC', '3', 'CLICK', '2019-08-06 10:05:32')
INTO user_interaction VALUES ( '9676A6E7-F77B-43BC-8604-1D750F69AC4C', '3', 'CLICK', '2019-08-06 10:05:35')
INTO user_interaction VALUES ( 'E1CF20D9-B850-4754-ABD3-F941F8B1F2DB', '3', 'CLICK', '2019-08-06 10:05:36')
INTO user_interaction VALUES ( 'DAC09DBF-9CFA-4A2F-97E4-F1692BD9DCFA', '3', 'VIEW', '2019-08-06 10:05:39')
INTO user_interaction VALUES ( '6E9B39A5-8E74-438F-8930-BB23E0DBD83C', '3', 'CLICK', '2019-08-06 10:05:47')
INTO user_interaction VALUES ( 'B5746053-A43A-478C-8241-36310CC7357C', '3', 'EXIT', '2019-08-06 10:05:57')
INTO user_interaction VALUES ( '229DDC45-698A-4755-889E-2736C7A812AF', '1', 'CLICK', '2019-08-07 10:05:13')
INTO user_interaction VALUES ( '2ECE624E-64BD-4EEC-9630-B4AC89473713', '1', 'VIEW', '2019-08-07 10:05:23')
INTO user_interaction VALUES ( 'E3A73FFD-6A83-482F-818A-FCF96FA8841E', '1', 'CLICK', '2019-08-07 10:05:25')
INTO user_interaction VALUES ( '4016C917-12DF-4D08-8812-6EEA28D78A05', '1', 'VIEW', '2019-08-07 10:05:25')
INTO user_interaction VALUES ( '82B7244A-6448-4AC5-8B1A-6F8E54F3B368', '1', 'CLICK', '2019-08-07 10:05:29')
INTO user_interaction VALUES ( 'DC3B3026-6429-4897-A1AD-F7D8846510D7', '1', 'CLICK', '2019-08-07 10:05:37')
INTO user_interaction VALUES ( '16AB45F8-1656-4D6F-BDA3-EB94B180A641', '1', 'CLICK', '2019-08-07 10:05:49')
INTO user_interaction VALUES ( '36172EF9-1F1D-4407-8050-D4400D4E0550', '1', 'CLICK', '2019-08-07 10:05:59')
INTO user_interaction VALUES ( '3C51C7F1-2E34-42E1-9151-1FC5BAB8CDDF', '1', 'CLICK', '2019-08-07 10:06:01')
INTO user_interaction VALUES ( 'EE387CBF-0305-4438-8BFB-A244539BA989', '1', 'VIEW', '2019-08-07 10:06:03')
INTO user_interaction VALUES ( '29D45D81-17E5-433B-86F4-69197D1DFAFA', '1', 'CLICK', '2019-08-07 10:06:09')
INTO user_interaction VALUES ( 'BAAD6B0D-B51F-4168-818A-B2010DCCCB42', '1', 'CLICK', '2019-08-07 10:06:14')
INTO user_interaction VALUES ( '06DD0F60-147E-4060-8E58-C9C15C96E1BB', '1', 'VIEW', '2019-08-07 10:06:25')
INTO user_interaction VALUES ( '8DE6DF58-B03A-49CF-B947-C0BBEF1655FC', '1', 'CLICK', '2019-08-07 10:06:29')
INTO user_interaction VALUES ( '2AF6654C-64D0-4808-A894-CB39376F313D', '1', 'CLICK', '2019-08-07 10:06:45')
INTO user_interaction VALUES ( 'D6C1714C-8038-436B-82E1-37F836651149', '1', 'CLICK', '2019-08-07 10:06:46')
INTO user_interaction VALUES ( 'F5F66A64-7302-4AEE-88B6-ED2EF55EB996', '1', 'VIEW', '2019-08-07 10:06:47')
INTO user_interaction VALUES ( 'E5C2B00C-574C-441C-B62A-48C44B97E7FC', '1', 'CLICK', '2019-08-07 10:06:50')
INTO user_interaction VALUES ( 'D6EDEF54-6800-4394-971A-E9A001722E73', '1', 'EXIT', '2019-08-07 10:06:55')
SELECT 1 FROM DUAL;
COMMIT;
  
--------------------------------------------------------------------------------------------------------------------------------
--Q1:
--We’ve noticed that there’s something wrong with the event logging: it sometimes logs a wrong user_id which does not even exist in our database. 
--We suspect that the problem is only related to some event types. 
--List the event types where the problem occurs the most often.

--Solution:
--Review columns in USERS TABLE, USER_INTERACTION to understand what these 2 tables contain:
SELECT * FROM USERS;
SELECT * FROM USER_INTERACTION;

--List all event types where problem occurs:
--Steps:
    --1.identify which user_id exists in event table but not in user table: (in DAT0)
    --2.Count no of event types occur for these nonexisted users
WITH DAT0 AS(
SELECT DISTINCT USER_ID FROM USER_INTERACTION A
WHERE NOT EXISTS (SELECT 1 FROM USERS B WHERE A.USER_ID=B.USER_ID)
)
SELECT DISTINCT DAT1.EVENT_TYPE, COUNT(*)
FROM USER_INTERACTION DAT1
LEFT JOIN DAT0 ON DAT1.USER_ID = DAT0.USER_ID
WHERE DAT0.USER_ID IS NOT NULL
GROUP BY DAT1.EVENT_TYPE
ORDER BY COUNT(*) DESC
;


--Result:
EVENT_TYPE	COUNT(*)
VIEW		2
CLICK		1

--------------------------------------------------------------------------------------------------------------------------------

--Q2:
--We have spotted that we’ve had a lot of events duplicated on a specific date. What date was that? 📅

--Solution:
    --1.Find all event ids which occur many times at the same event time in the interaction table
SELECT EVENT_ID, EVENT_TIME, SUBSTR(EVENT_TIME,1,10) EVENT_DATE, COUNT(*)
FROM user_interaction A
GROUP BY EVENT_ID, EVENT_TIME
HAVING COUNT(*)>1
;


--Result:
EVENT_ID				EVENT_TIME		EVENT_DATE	COUNT(*)
F36AABE3-0306-6669-36F0-5E6C255553FD	2019-05-05 14:05:23	2019-05-05	2
5F9CF4D8-6D12-331C-1224-109D188E9918	2019-05-05 14:05:24	2019-05-05	2

--------------------------------------------------------------------------------------------------------------------------------
--Q3:
--What country do most of our website visitors come from? 🌐

--Solution:
    --1.Take user interaction as the main table, identify countries associated with each user by left joining with user table (Done in With Dat0)
    --2.Count Event ID as it represents the website visit
WITH DAT0 AS(
SELECT  DISTINCT A.*, NVL(B.COUNTRY,'N/A') COUNTRY
FROM USER_INTERACTION A
LEFT JOIN USERS B ON A.USER_ID=B.USER_ID
)
SELECT DAT0.COUNTRY, COUNT(EVENT_ID) NO_OF_VISIT
FROM DAT0
GROUP BY DAT0.COUNTRY
ORDER BY COUNT(EVENT_ID) DESC
;


--Result:
COUNTRY		NO_OF_VISIT
DE		74
UK		7
N/A		3

--------------------------------------------------------------------------------------------------------------------------------
--Q4:
--Our most valuable users are the ones who have completed a minimum of 5 VIEWs and 10 CLICKs. What age group do they fall in: 18-49 or 50-80?

--Solution:
    --1.Identify a list of all user id with event types and their counts which satisfies the condition of having minimum 5 Views or minimum of 10 Clicks:
    --2.Pivot the result in step 1 with Event type as column then create conditions where "'Click'" and "'View'" both have to be not null
    
SELECT * FROM (
WITH DAT0 AS(
SELECT DISTINCT USER_ID, EVENT_TYPE, COUNT(EVENT_TYPE) COUNT_EVENT FROM USER_INTERACTION
GROUP BY USER_ID, EVENT_TYPE
ORDER BY USER_ID
)
SELECT DAT0.USER_ID, DAT0.EVENT_TYPE, DAT0.COUNT_EVENT
FROM DAT0
WHERE ((DAT0.EVENT_TYPE ='VIEW' AND DAT0.COUNT_EVENT>=5) OR (DAT0.EVENT_TYPE ='CLICK' AND DAT0.COUNT_EVENT>=10))
)
PIVOT
(MAX(COUNT_EVENT) FOR EVENT_TYPE IN ('CLICK','VIEW')
) 
WHERE "'CLICK'" IS NOT NULL AND "'VIEW'" IS NOT NULL
ORDER BY USER_ID
;


--Result:
USER_ID		'CLICK'		'VIEW'
1		35		12
3		16		5

--------------------------------------------------------------------------------------------------------------------------------

--Q5:
--What do you think of the query below? Share your thoughts in a few sentences.

create table account_invoice_records as
select
	  ih.invoice_number
	, ih.account_number
	, ih.invoice_created_date
	, ih.invoice_due_date
	, ii.product_number
	, ii.net_amount
	, ii.tax_amount
from invoice_headers ih   -- 10m+ records
	join invoice_items ii   -- 100m+ records
where 1=2
and ih.account_number = 1234
;

--Solution:
--The query above contains errors so wont be executed. Errors are below:
--Missing "ON" after Invoice_items ii
--really want to create a table with values generated from the query or just table structure?


--------------------------------------------------------------------------------------------------------------------------------

--Q6:
--Create a matrix with these parameters: Rows: user country , Columns: user age, Measure: count of interactions

--Solution:
CREATE TABLE INTERACTION_BY_COUNTRY_AGE AS(
SELECT * FROM
(
  SELECT DISTINCT B.AGE, B.COUNTRY, A.EVENT_ID
    FROM USER_INTERACTION A
    LEFT JOIN USERS B ON A.USER_ID=B.USER_ID
)
PIVOT
(
  COUNT(DISTINCT EVENT_ID)
  FOR (AGE)
  IN (('18-45'), ('46-80'))
)WHERE COUNTRY IS NOT NULL
)
;


--Result:
COUNTRY		'18-45'		'46-80'
DE		23		51
UK		5		2

--------------------------------------------------------------------------------------------------------------------------------

--Q7:
--Create a table which shows the user interactions and the previous interaction event for every record. Display the previous event_type and event_time.
--Not too sure about this question, so the table structure might be off

--Solution:
CREATE TABLE INTERACTION_BY_USER AS (
SELECT * FROM
(
  SELECT USER_ID, EVENT_TIME, EVENT_TYPE, EVENT_ID
    FROM USER_INTERACTION
)
PIVOT
(
  COUNT(DISTINCT EVENT_ID)
  FOR (EVENT_TYPE)
  IN (('CLICK'), ('VIEW'), ('EXIT'))
)
)
;

--Result:
USER_ID		EVENT_TIME		'CLICK'		'VIEW'		'EXIT'
1		2019-05-05 14:05:25	1		0		0
1		2019-07-05 10:05:25	1		0		0
1		2019-07-05 10:05:26	2		0		0
1		2019-07-05 10:05:29	0		1		0
1		2019-07-05 10:05:31	1		0		0
1		2019-07-05 10:05:46	0		0		1
1		2019-07-07 10:05:13	1		0		0
1		2019-07-07 10:05:23	0		1		0
1		2019-07-07 10:05:25	1		1		0
1		2019-07-07 10:05:29	1		0		0
1		2019-07-07 10:05:37	1		0		0
1		2019-07-07 10:05:49	1		0		0
1		2019-07-07 10:05:59	1		0		0
1		2019-07-07 10:06:01	1		0		0
1		2019-07-07 10:06:03	0		1		0
1		2019-07-07 10:06:09	1		0		0
1		2019-07-07 10:06:14	1		0		0
1		2019-07-07 10:06:25	0		1		0
1		2019-07-07 10:06:29	1		0		0
1		2019-07-07 10:06:45	1		0		0
1		2019-07-07 10:06:46	1		0		0
1		2019-07-07 10:06:47	0		1		0
1		2019-07-07 10:06:50	1		0		0
1		2019-07-07 10:06:55	0		0		1
1		2019-08-05 10:05:25	1		0		0
1		2019-08-05 10:05:26	2		0		0
1		2019-08-05 10:05:29	0		1		0
1		2019-08-05 10:05:31	1		0		0
1		2019-08-05 10:05:46	0		0		1
1		2019-08-07 10:05:13	1		0		0
1		2019-08-07 10:05:23	0		1		0
1		2019-08-07 10:05:25	1		1		0
1		2019-08-07 10:05:29	1		0		0
1		2019-08-07 10:05:37	1		0		0
1		2019-08-07 10:05:49	1		0		0
1		2019-08-07 10:05:59	1		0		0
1		2019-08-07 10:06:01	1		0		0
1		2019-08-07 10:06:03	0		1		0
1		2019-08-07 10:06:09	1		0		0
1		2019-08-07 10:06:14	1		0		0
1		2019-08-07 10:06:25	0		1		0
1		2019-08-07 10:06:29	1		0		0
1		2019-08-07 10:06:45	1		0		0
1		2019-08-07 10:06:46	1		0		0
1		2019-08-07 10:06:47	0		1		0
1		2019-08-07 10:06:50	1		0		0
1		2019-08-07 10:06:55	0		0		1
3		2019-05-05 14:05:20	1		0		0
3		2019-05-05 14:05:21	0		1		0
3		2019-05-05 14:05:22	1		0		0
3		2019-07-06 10:05:10	1		0		0
3		2019-07-06 10:05:14	1		0		0
3		2019-07-06 10:05:16	0		1		0
3		2019-07-06 10:05:17	1		0		0
3		2019-07-06 10:05:32	1		0		0
3		2019-07-06 10:05:35	1		0		0
3		2019-07-06 10:05:36	1		0		0
3		2019-07-06 10:05:39	0		1		0
3		2019-07-06 10:05:47	1		0		0
3		2019-07-06 10:05:57	0		0		1
3		2019-08-06 10:05:10	1		0		0
3		2019-08-06 10:05:14	1		0		0
3		2019-08-06 10:05:16	0		1		0
3		2019-08-06 10:05:17	1		0		0
3		2019-08-06 10:05:32	1		0		0
3		2019-08-06 10:05:35	1		0		0
3		2019-08-06 10:05:36	1		0		0
3		2019-08-06 10:05:39	0		1		0
3		2019-08-06 10:05:47	1		0		0
3		2019-08-06 10:05:57	0		0		1
45		2019-05-05 14:05:23	0		1		0
45		2019-05-05 14:05:24	0		0		1
80		2019-05-05 14:10:26	1		0		0
80		2019-05-05 14:10:29	0		1		0
80		2019-05-05 14:10:31	0		1		0
100		2019-05-05 14:05:26	1		0		0
100		2019-06-05 09:05:12	1		0		0
100		2019-06-05 09:05:13	0		1		0
100		2019-06-05 09:05:16	1		0		0
100		2019-06-05 09:05:21	0		0		1

--------------------------------------------------------------------------------------------------------------------------------

--Q8:
--We need to identify the first and the last interaction for every day. Show the whole record for the first and last interactions for each day.

--Solution 1. If want to see 1st and last interaction drilling down to only @ day level, the use this code below:
SELECT * FROM (
WITH DAT0 AS(
SELECT SUBSTR(EVENT_TIME,1,10) DATE1, SUBSTR(EVENT_TIME,11,9) TIME1, EVENT_TYPE, ROW_NUMBER() OVER(PARTITION BY SUBSTR(EVENT_TIME,1,10) ORDER BY SUBSTR(EVENT_TIME,11,9) DESC) RN_LAT
FROM USER_INTERACTION
),
DAT1 AS(
SELECT SUBSTR(EVENT_TIME,1,10) DATE1, SUBSTR(EVENT_TIME,11,9) TIME1, EVENT_TYPE, ROW_NUMBER() OVER(PARTITION BY SUBSTR(EVENT_TIME,1,10) ORDER BY SUBSTR(EVENT_TIME,11,9) ASC) RN_EAR
FROM USER_INTERACTION
)
SELECT * FROM (
SELECT DAT0.DATE1, 'LAST INTERACTION' AS INTERACTION, DAT0.EVENT_TYPE FROM DAT0 WHERE RN_LAT=1
UNION ALL
SELECT DAT1.DATE1, 'FIRST INTERACTION' AS INTERACTION, DAT1.EVENT_TYPE FROM DAT1 WHERE RN_EAR=1
) ORDER BY DATE1
)
PIVOT (
MAX(EVENT_TYPE) FOR INTERACTION IN ('FIRST INTERACTION','LAST INTERACTION')
) ORDER BY DATE1
;

--Result:
DATE1		'FIRST INTERACTION' 	'SECOND INTERACTION'
2019-05-05	CLICK			VIEW
2019-06-05	CLICK			EXIT
2019-07-05	CLICK			EXIT
2019-07-06	CLICK			EXIT
2019-07-07	CLICK			EXIT
2019-08-05	CLICK			EXIT
2019-08-06	CLICK			EXIT
2019-08-07	CLICK			EXIT



--Solution 2. If want to see 1st and last interaction drilling down to User_id, the use this code below
--Identify Event_type by date and time clustering in event type at latest time in a day union with event type at earliest time in a day
SELECT * FROM (
WITH DAT0 AS(
SELECT USER_ID, SUBSTR(EVENT_TIME,1,10) DATE1, SUBSTR(EVENT_TIME,11,9) TIME1, EVENT_TYPE, ROW_NUMBER() OVER(PARTITION BY SUBSTR(EVENT_TIME,1,10), USER_ID ORDER BY SUBSTR(EVENT_TIME,11,9) DESC) RN_LAT
FROM USER_INTERACTION
),
DAT1 AS(
SELECT USER_ID, SUBSTR(EVENT_TIME,1,10) DATE1, SUBSTR(EVENT_TIME,11,9) TIME1, EVENT_TYPE, ROW_NUMBER() OVER(PARTITION BY SUBSTR(EVENT_TIME,1,10), USER_ID ORDER BY SUBSTR(EVENT_TIME,11,9) ASC) RN_EAR
FROM USER_INTERACTION
)
SELECT * FROM (
SELECT DAT0.USER_ID, DAT0.DATE1, 'LAST INTERACTION' AS INTERACTION, DAT0.EVENT_TYPE FROM DAT0 WHERE RN_LAT=1
UNION ALL
SELECT DAT1.USER_ID, DAT1.DATE1, 'FIRST INTERACTION' AS INTERACTION, DAT1.EVENT_TYPE FROM DAT1 WHERE RN_EAR=1
) ORDER BY DATE1
)
PIVOT (
MAX(EVENT_TYPE) FOR INTERACTION IN ('FIRST INTERACTION','LAST INTERACTION')
) ORDER BY USER_ID
;

--------------------------------------------------------------------------------------------------------------------------------

--Q9:
--Describe the result of this query. What is it used for? 🤔
with xu as (select                                  
                  u.user_id
                , u.user_email
            from sample.user u
                join sample.user_plan up 
                    on u.user_id=up.user_id 
                        and up.date_from < current_date
                        and up.date_to >= current_date
                join sample.plan p
                    on up.plan_id=p.plan_id 
                        and p.plan_type='free')
    , xel (select
            xu.user_id
            , el.client_ip_address as last_client_ip_address
            , date_trunc('DAY', el.event_datetime) as last_event_date_day
            , row_number() over (partition by xu.user_id order by el.event_datetime desc) as rn
          from xu
            join sample.event el
                on xu.user_id=el.user_id)
    , xer (select
            xu.user_id
           , date_trunc('DAY',max(erevent_datetime)) as last_registration_date_day
          from xu
            join sample.event er
                on xu.user_id=er.user_id
                    and er.event_type='registration'
          group by xu.user_id)            
select 
    xu.user_email
    , coalesce(ig.country,'N/A') as ip_country
from xu
    join xel 
        on xu.user_id=xel.user_id
    join xer
        on xu.user_id=xer.user_id
    left outer join ip_geo ig
        on xel.last_client_ip_address=ig.ip_address
where 1=1
    and xel.rn = 1
    and xel.last_event_date_day = xer.last_registration_date_day
    and coalesce(ig.country,'N/A') in ('UK','N/A')
    and xu.user_email ilike '%@gmail.com'
;

--Solution: table shows user_email and ip country of people who are either in UK or 'NA' that most recently registered (have the lastest event is "registration") and their emails are gmail


